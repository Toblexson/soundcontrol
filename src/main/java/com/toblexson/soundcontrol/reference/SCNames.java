package com.toblexson.soundcontrol.reference;

public class SCNames 
{
	public static final class Items
	{
		public static final String VINYL_NAME = "vinyl";
		public static final String DISC_CORE_NAME = "discCore";
	}
	
	public static final class Blocks
	{
		public static final String DISC_CASE_NAME = "discCase";
	}
}
