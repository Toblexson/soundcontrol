package com.toblexson.soundcontrol.block;

import com.toblexson.soundcontrol.init.SCItems;
import com.toblexson.soundcontrol.reference.SCInfo;
import com.toblexson.soundcontrol.reference.SCNames;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyDirection;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class BlockDiscCase extends BlockContainer
{
	public static final PropertyDirection FACING = PropertyDirection.create("facing", EnumFacing.Plane.HORIZONTAL);
	
	public BlockDiscCase()
	{
		super(Material.wood);
		this.stepSound = soundTypeWood;
		this.setCreativeTab(CreativeTabs.tabDecorations);
		this.setUnlocalizedName(SCInfo.MOD_ID + "_" + SCNames.Blocks.DISC_CASE_NAME);
	}
	
	/*@Override
	protected BlockState createBlockState() 
	{
		return new BlockState(this, new IProperty[]{FACING});
	}*/
	
	public String getName()
	{
		return SCNames.Blocks.DISC_CASE_NAME;
	}
	
	public String getTextureLocation()
	{
		return SCInfo.MOD_ID + ":" + getName();
	}
	
	@Override
	public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) 
	{
		if(!worldIn.isRemote)
		{
			
		}
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		// TODO Auto-generated method stub
		return null;
	}
}
