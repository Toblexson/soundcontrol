package com.toblexson.soundcontrol.item;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.toblexson.soundcontrol.reference.SCInfo;
import com.toblexson.soundcontrol.reference.SCNames;

public class ItemDiscCore extends Item
{
	public ItemDiscCore()
	{
		setUnlocalizedName(SCInfo.MOD_ID + "_" + SCNames.Items.VINYL_NAME);
		setCreativeTab(CreativeTabs.tabMisc);
		setHasSubtypes(true);
	}
	
	public String getName(int meta)
	{
		return SCNames.Items.DISC_CORE_NAME + meta;
	}
	
	public String getTextureLocation(int meta)
	{
		return SCInfo.MOD_ID + ":" + this.getName(meta);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack) 
	{
		return this.getUnlocalizedName() + ":" + stack.getItemDamage();
	}
	
	@Override
	public void getSubItems(Item itemIn, CreativeTabs tab, List subItems) 
	{
		for(int meta = 0; meta < 11; meta++)
		{
			subItems.add(new ItemStack(itemIn, 1, meta));
		}
	}
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn, List tooltip, boolean advanced) 
	{
		switch(stack.getItemDamage())
		{
		case 0:
			tooltip.add("13");
			break;
		case 1:
			tooltip.add("cat");
			break;
		case 2:
			tooltip.add("blocks");
			break;
		case 3:
			tooltip.add("chirp");
			break;
		case 4:
			tooltip.add("far");
			break;
		case 5:
			tooltip.add("mall");
			break;
		case 6:
			tooltip.add("mellohi");
			break;
		case 7:
			tooltip.add("stal");
			break;
		case 8:
			tooltip.add("strad");
			break;
		case 9:
			tooltip.add("ward");
			break;
		case 10:
			tooltip.add("wait");
			break;
		}
	}
}
