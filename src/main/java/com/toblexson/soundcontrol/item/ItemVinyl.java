package com.toblexson.soundcontrol.item;

import java.util.List;

import com.toblexson.soundcontrol.reference.SCInfo;
import com.toblexson.soundcontrol.reference.SCNames;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemVinyl extends Item
{	
	public ItemVinyl()
	{
		setUnlocalizedName(SCInfo.MOD_ID + "_" + SCNames.Items.VINYL_NAME);
		setCreativeTab(CreativeTabs.tabMisc);
		setHasSubtypes(true);
	}
	
	public String getName(int meta) 
	{
		return SCNames.Items.VINYL_NAME + (meta == 1 ? "Dust" : "");
	}
	
	public String getTextureLocation(int meta)
	{
		return SCInfo.MOD_ID + ":" + this.getName(meta);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack) 
	{
		return this.getUnlocalizedName() + (stack.getItemDamage() == 1 ? ":dust" : "");
	}
	
	@Override
	public void getSubItems(Item itemIn, CreativeTabs tab, List subItems) 
	{
		subItems.add(new ItemStack(itemIn, 1, 0));
		subItems.add(new ItemStack(itemIn, 1, 1));
	}
}
