package com.toblexson.soundcontrol;

import com.toblexson.soundcontrol.init.SCBlocks;
import com.toblexson.soundcontrol.init.SCItems;
import com.toblexson.soundcontrol.init.SCRecipes;
import com.toblexson.soundcontrol.reference.SCInfo;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = SCInfo.MOD_ID, version = SCInfo.VERSION)
public class SoundControl 
{
	@Instance(SCInfo.MOD_ID)
	public static SoundControl instance;
	
	@EventHandler
	public static void preInit(FMLPreInitializationEvent event)
	{
		SCItems.init();
		SCBlocks.init();
	}
	
	@EventHandler
	public static void init(FMLInitializationEvent event)
	{
		//initialising textures
		if(event.getSide() == Side.CLIENT)
		{
			SCItems.render();
			SCBlocks.render();
		}
		
		SCRecipes.Items();
	}
	
	@EventHandler
	public static void postInit(FMLPostInitializationEvent event)
	{}
}
