package com.toblexson.soundcontrol.init;

import com.toblexson.soundcontrol.block.BlockDiscCase;
import com.toblexson.soundcontrol.reference.SCNames;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class SCBlocks 
{
	private static final Block discCase = new BlockDiscCase();
	
	public static void init()
	{
		GameRegistry.registerBlock(discCase, SCNames.Blocks.DISC_CASE_NAME);
	}
	
	public static void render()
	{
		RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
		
		renderItem.getItemModelMesher().register(Item.getItemFromBlock(discCase), 0, new ModelResourceLocation(((BlockDiscCase)discCase).getTextureLocation(), "inventory"));
	}
}
