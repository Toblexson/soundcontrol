package com.toblexson.soundcontrol.init;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class SCRecipes 
{
	public static void Items()
	{
		Vinyl();
		DiscCore();
		MusicDisc();
	}
	
	private static void Vinyl()
	{
		//vinyl
		GameRegistry.addSmelting(new ItemStack(SCItems.vinyl, 1, 1), new ItemStack(SCItems.vinyl, 1, 0), 0.5F);
		
		//vinyl dust
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(SCItems.vinyl, 1, 1), new Object[]
			{
				"ingotIron", 
				Blocks.obsidian,
				Items.redstone
			}));
	}
	
	private static void DiscCore()
	{
		//Disc Core 0 (13)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 0), new Object[]
			{
				"yyy",
				"ydw",
				"www",
				'y', "dyeYellow",
				'd', Items.diamond,
				'w', "dyeWhite"
			}));
		//Disc Core 1 (Cat)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 1), new Object[]
			{
				"lll",
				"gdg",
				"lll",
				'l', "dyeLime",
				'd', Items.diamond,
				'g', "dyeGreen"
			}));
		//Disc Core 2 (Blocks)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 2), new Object[]
			{
				"rrr",
				"rdr",
				"rrr",
				'r', Items.brick,
				'd', Items.diamond,
			}));
		//Disc Core 3 (Chirp)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 3), new Object[]
			{
				"rnn",
				"rdr",
				"nnr",
				'r', "dyeRed",
				'd', Items.diamond,
				'n', Items.netherbrick
			}));
		//Disc Core 4 (Far)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 4), new Object[]
			{
				"lcl",
				"cdc",
				"lcl",
				'l', "dyeLime",
				'd', Items.diamond,
				'c', Items.slime_ball
			}));
		//Disc Core 5 (Mall)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 5), new Object[]
			{
				"ppp",
				"bdb",
				"ppp",
				'p', "dyePurple",
				'd', Items.diamond,
				'b', "dyeBlue"
			}));
		//Disc Core 6 (Mellohi)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 6), new Object[]
			{
				"mwm",
				"mdm",
				"mwm",
				'm', "dyeMagenta",
				'd', Items.diamond,
				'w', "dyeWhite"
			}));
		//Disc Core 7 (Stal)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 7), new Object[]
			{
				"bbb",
				"bdb",
				"bbb",
				'b', "dyeBlack",
				'd', Items.diamond,
			}));
		//Disc Core 8 (Strad)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 8), new Object[]
			{
				"www",
				"wdw",
				"www",
				'd', Items.diamond,
				'w', "dyeWhite"
			}));
		//Disc Core 9 (Ward)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 9), new Object[]
			{
				"ggg",
				"gdl",
				"lll",
				'g', "dyeGreen",
				'd', Items.diamond,
				'l', "dyeLime"
			}));
		//Disc Core 10 (Wait)
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(SCItems.discCore, 1, 10), new Object[]
			{
				"bbb",
				"bdb",
				"bbb",
				'b', "dyeLightBlue",
				'd', Items.diamond,
			}));
	}
	
	private static void MusicDisc()
	{
		//Record 13
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_13), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 0)
			});
		//Record Cat
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_cat), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 1)
			});
		//Record Blocks
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_blocks), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 2)
			});
		//Record Chirp
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_chirp), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 3)
			});
		//Record Far
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_far), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 4)
			});
		//Record Mall
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_mall), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 5)
			});
		//Record Mellohi
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_mellohi), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 6)
			});
		//Record Stal
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_stal), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 7)
			});
		//Record Strad
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_strad), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 8)
			});
		//Record Ward
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_ward), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 9)
			});
		//Record Wait
		GameRegistry.addShapedRecipe(new ItemStack(Items.record_wait), new Object[] 
			{
				"vvv",
				"vcv",
				"vvv",
				'v', SCItems.vinyl,
				'c', new ItemStack(SCItems.discCore, 1, 10)
			});
		//Record 11
		GameRegistry.addShapelessRecipe(new ItemStack(Items.record_stal), new Object[] 
			{
				Items.record_stal
			});
	}
}
