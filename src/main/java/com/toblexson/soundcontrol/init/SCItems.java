package com.toblexson.soundcontrol.init;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

import com.toblexson.soundcontrol.item.ItemDiscCore;
import com.toblexson.soundcontrol.item.ItemVinyl;
import com.toblexson.soundcontrol.reference.SCInfo;
import com.toblexson.soundcontrol.reference.SCNames;

public class SCItems
{
	public static final Item vinyl = new ItemVinyl();
	public static final Item discCore = new ItemDiscCore();
	
	public static void init()
	{
		GameRegistry.registerItem(vinyl, SCNames.Items.VINYL_NAME);
		GameRegistry.registerItem(discCore, SCNames.Items.DISC_CORE_NAME);

		ModelBakery.addVariantName(vinyl, ((ItemVinyl)vinyl).getTextureLocation(0), ((ItemVinyl)vinyl).getTextureLocation(1));
		ModelBakery.addVariantName(discCore, ((ItemDiscCore)discCore).getTextureLocation(0), ((ItemDiscCore)discCore).getTextureLocation(1), 
				((ItemDiscCore)discCore).getTextureLocation(2), ((ItemDiscCore)discCore).getTextureLocation(3), ((ItemDiscCore)discCore).getTextureLocation(4),
				((ItemDiscCore)discCore).getTextureLocation(5), ((ItemDiscCore)discCore).getTextureLocation(6), ((ItemDiscCore)discCore).getTextureLocation(7),
				((ItemDiscCore)discCore).getTextureLocation(8), ((ItemDiscCore)discCore).getTextureLocation(9), ((ItemDiscCore)discCore).getTextureLocation(10));
	}
	
	public static void render()
	{
		RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
		
		renderItem.getItemModelMesher().register(vinyl, 0, new ModelResourceLocation(((ItemVinyl)vinyl).getTextureLocation(0), "inventory"));
		renderItem.getItemModelMesher().register(vinyl, 1, new ModelResourceLocation(((ItemVinyl)vinyl).getTextureLocation(1), "inventory"));
	
		for(int meta = 0; meta < 11; meta ++)
		{
			renderItem.getItemModelMesher().register(discCore, meta, new ModelResourceLocation(((ItemDiscCore)discCore).getTextureLocation(meta), "inventory"));
		}
	}
}
